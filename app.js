// Activity:

// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
//     - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
//         (target.health - this.attack)

// 2.) If health is below 5, invoke faint function.



function Pokemon(name, level) {
    // properties
    this.name = name;
    this.level = level;
    this.health = 4 * level;
    this.attack = level / 2;

    // method
    this.tackle = function (target) {
        let critical = Math.floor(Math.random() * 6) + 1;
        if (critical > 3) {
            console.warn(this.name + " tackled " + target.name + ". Critical hit damage of" + (this.attack * 2) + "!")
            console.log(target.name + "'s health is now " + (target.health - (this.attack * 2)) + ".")
            target.health = (target.health - (this.attack * 2))
            if (target.health < 5) {
                target.faint()
            }
        }
        else {
            console.log(this.name + " tackled " + target.name + ". " + target.name + "'s health decreased by " + this.attack + ".")
            console.log(target.name + "'s health is now " + (target.health - this.attack) + ".")
            target.health = (target.health - this.attack)
            if (target.health < 5) {
                target.faint()
            }
        }

    };
    this.faint = function () {
        console.warn(this.name + " fainted.")
    }
}


let dragonite = new Pokemon("Dragonite", 66)
let blastoise = new Pokemon("Blastoise", 59)

console.log(dragonite)
console.log(blastoise)

